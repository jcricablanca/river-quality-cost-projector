library(shiny) #install shiny package for R to run program

shinyUI(fluidPage(
  
  # Application title
  titlePanel("River Quality Cost Projector"),
  
  sidebarLayout(
    sidebarPanel(checkboxGroupInput("RivStations", "River Stations:",
                              c("500m to MeyMZ" = 1,
                                "Caingin Bridge" = 2,
                                "Expressway Bridge" = 3,
                                "McArthur Bridge II" = 4,
                                "McArthur Bridge" = 5,
                                "Meyc-Mar Mixing Zone" = 6,
                                "Meymart Bridge" = 7,
                                "Perez Bridge" = 8,
                                "Polo Bridge" = 9,
                                "Tawiran Bridge" = 10,
                                "Viente Reales Bridge" = 11)),
                 radioButtons("edit", NULL, 
                              c("Stations" = "stat", 
                                "Edit River Quality Components Cost and Remediation" = "rqcar",
                                "Edit River Quality Component Amounts in Stations" = "rqcas")),
                 radioButtons("goal", "Standard Cases:",
                              c("Minimal Standard Case" = "min",
                                "Maximal Standard Case" = "max")),
                 actionButton("run", "Generate")
    ),
    mainPanel(
      conditionalPanel(
        condition = "input.edit == 'rqcar'",
        selectInput(
          "RQC1", "River Quality Components",
          c("Arsenic" = 1,"Cadmium" = 2,"Chromium 6+" = 3,"Copper" = 4,"Lead" = 5,"Mercury" = 6)),
        actionButton("savecar", "Save"),
        textInput("cost","Cost of Remediation in 1L of Water (PhP)"),
        textInput("remd","Amount of Remediation in 1L of Water (mg/L)"),
        textInput("minstdr","Minimum Standard Requirement (mg/L)"),
        textInput("maxstdr","Maximum Standard Requirement (mg/L)")
      ),
        conditionalPanel(
          condition = "input.edit == 'rqcas'",
          selectInput(
            "RQC2", "River Quality Components",
            c("Arsenic" = "as","Cadmium" = "cd","Chromium 6+" = "cr","Copper" = "cu","Lead" = "pb","Mercury" = "hg")),
          actionButton("savecas", "Save"),
          br(),
          br(),
          textInput("rs1","500m to MeyMZ"),
          textInput("rs2","Caingin Bridge"),
          textInput("rs3","Expressway Bridge"),
          textInput("rs4","McArthur Bridge II"),
          textInput("rs5","McArthur Bridge"),
          textInput("rs6","Meyc-Mar Mixing Zone"),
          textInput("rs7","Meymart Bridge"),
          textInput("rs8","Perez Bridge"),
          textInput("rs9","Polo Bridge"),
          textInput("rs10","Tawiran Bridge"),
          textInput("rs11","Viente Reales Bridge")
        ),
      conditionalPanel(
        condition = "input.edit == 'stat'",
        imageOutput("image1"),
        textOutput("NULLcbox"),
        tableOutput("table")
      )
      
    )
)
))