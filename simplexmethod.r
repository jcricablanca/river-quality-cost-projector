SimplexMethod <- function(mat){
  
  negs = sum(mat[nrow(mat),] < 0) #count the negative of the last row
  iter = 0
  
  while(negs != 0){
    pivcol = which.min(mat[nrow(mat),]) #use which.min() to find index of pivot column
    minrow = 1000 #temporary minimum to find pivotrow
    
    for(i in 1:nrow(mat)){  #algorithm to find the index of the pivot row
      if(mat[i, pivcol] > 0){
        tempmin = mat[i,ncol(mat)] / mat[i,pivcol] #tempmin stores the test ratio to find the pivot row
        if(tempmin < minrow){ #update minrow if there is a smaller test ratio 
          minrow = tempmin
          pivrow = i # i is now the pivrow
        }
      }
    }
    
    #normalize pivot row
    mat[pivrow,] = mat[pivrow,] / mat[pivrow,pivcol]
    
    #"GaussJordan" part
    for(i in 1:nrow(mat)){
      if(i != pivrow){
        x = mat[i, pivcol]/mat[pivrow, pivcol]
        temp = mat[pivrow, ]*x
        mat[i,] = mat[i,] - temp
      }
    }
    #update negs for negatives in last row
    negs = sum(mat[nrow(mat),] < 0)
    iter = iter+1
    labl = paste("iterations/iteration_" , iter,".csv")
    write.table(mat, file = labl, sep = ",", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = FALSE)
  }
  
  return(mat)
}