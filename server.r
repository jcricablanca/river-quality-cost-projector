library(shiny)

# Define server logic
shinyServer(function(input, output, session) {
    source('simplexmethod.r', local = TRUE)  
  
    casmat <- matrix(0, ncol = 6, nrow = 11) #Amount of RQC in all stations matrix
    casmat[1,] = c(10.76, 9.99, 251, 290.58, 321.62, 6.99)
    casmat[2,] = c(12.69, 9.99, 457.36, 1057.36, 351.16, 6.99)
    casmat[3,] = c(8.99, 9.99, 186.26, 78.12, 229.51, 6.99)
    casmat[4,] = c(8.99, 9.99, 261.13, 339.51, 697.47, 6.99)
    casmat[5,] = c(8.99, 9.99, 200.28, 42.22, 70.95, 6.99)
    casmat[6,] = c(8.99, 9.99, 193.09, 81.24, 99.02, 6.99)
    casmat[7,] = c(12.47, 9.99, 321.59, 329.87, 368.47, 6.99)
    casmat[8,] = c(8.99, 9.99, 45.12, 14.78, 1264.7, 6.99)
    casmat[9,] = c(8.99, 9.99, 208.76, 87.15, 216, 6.99)
    casmat[10,] = c(8.99, 5.5, 98.88, 28.57, 44.83, 6.99)
    casmat[11,] = c(22.61, 9.99, 508.61, 270.76, 1301.81, 6.99)
    
    carmat <- matrix(0, ncol = 4, nrow = 6) #Cost, min and max standard requirement, and remediation matrix
    carmat[1,] = c(0.06, 0.04,	0,	0.05)
    carmat[2,] = c(0.35,	0.08,	0,	0.01)
    carmat[3,] = c(1.47,	0.02,	0,	0.05)
    carmat[4,] = c(0.47,	0.04,	0,	0.05)
    carmat[5,] = c(0.23,	0.03,	0,	0.06)
    carmat[6,] = c(0.02,	0.45,	0,	0.002)
    
    volmat <- matrix(0, nrow = 11)
    volmat[1,] = 50000000
    volmat[2,] = 50500000
    volmat[3,] = 19850000
    volmat[4,] = 48750000
    volmat[5,] = 46000000
    volmat[6,] = 570000000
    volmat[7,] = 37000000
    volmat[8,] = 1440000
    volmat[9,] = 8010000
    volmat[10,] = 241500000
    volmat[11,] = 3540000
    
    matc <- reactiveValues()
    
    matc$matcas = casmat
    
    matc$matcar = carmat
    
    matc$forsimplex <- c()
    
    observeEvent(input$RQC2, {
      if(input$RQC2 == "as"){
        for(i in 1:11){
          retval = paste("rs", i, sep = "")
          updateTextInput(session, retval, value = matc$matcas[i,1])
        }
      }else if(input$RQC2 == "cd"){
        for(i in 1:11){
          retval = paste("rs", i, sep = "")
          updateTextInput(session, retval, value = matc$matcas[i,2])
        }
      }else if(input$RQC2 == "cr"){
        for(i in 1:11){
          retval = paste("rs", i, sep = "")
          updateTextInput(session, retval, value = matc$matcas[i,3])
        }
      }else if(input$RQC2 == "cu"){
        for(i in 1:11){
          retval = paste("rs", i, sep = "")
          updateTextInput(session, retval, value = matc$matcas[i,4])
        }
      }else if(input$RQC2 == "pb"){
        for(i in 1:11){
          retval = paste("rs", i, sep = "")
          updateTextInput(session, retval, value = matc$matcas[i,5])
        }
      }else{
        for(i in 1:11){
          retval = paste("rs", i, sep = "")
          updateTextInput(session, retval, value = matc$matcas[i,6])
        }
      }
    
    })
  
    observeEvent(input$savecas, {
      if(input$RQC2 == "as"){
        for(i in 1:11){
          textval = paste("rs", i, sep = "")
          matc$matcas[i,1] = as.numeric(input[[textval]])  
        }
      }else if(input$RQC2 == "cd"){
        for(i in 1:11){
          textval = paste("rs", i, sep = "")
          matc$matcas[i,2] = as.numeric(input[[textval]])  
        }
      }else if(input$RQC2 == "cr"){
        for(i in 1:11){
          textval = paste("rs", i, sep = "")
          matc$matcas[i,3] = as.numeric(input[[textval]])  
        }
      }else if(input$RQC2 == "cu"){
        for(i in 1:11){
          textval = paste("rs", i, sep = "")
          matc$matcas[i,4] = as.numeric(input[[textval]])  
        }
      }else if(input$RQC2 == "pb"){
        for(i in 1:11){
          textval = paste("rs", i, sep = "")
          matc$matcas[i,5] = as.numeric(input[[textval]])  
        }
      }else{
        for(i in 1:11){
          textval = paste("rs", i, sep = "")
          matc$matcas[i,6] = as.numeric(input[[textval]])  
        }
      } 
    })
    
   updt <- function(i){
     updateTextInput(session, "cost", value = matc$matcar[i,1])
     updateTextInput(session, "remd", value = matc$matcar[i,2])
     updateTextInput(session, "minstdr", value = matc$matcar[i,3])
     updateTextInput(session, "maxstdr", value = matc$matcar[i,4])
   }
   sav <- function(i){
     matc$matcar[i,1] = as.numeric(input[["cost"]])
     matc$matcar[i,2] = as.numeric(input[["remd"]])
     matc$matcar[i,3] = as.numeric(input[["minstdr"]])
     matc$matcar[i,4] = as.numeric(input[["maxstdr"]])
   }
   
   observeEvent(input$RQC1, {
     if(input$RQC1 == 1){
       updt(1)
     }else if(input$RQC1 == 2){
       updt(2)
     }else if(input$RQC1 == 3){
       updt(3)
     }else if(input$RQC1 == 4){
       updt(4)
     }else if(input$RQC1 == 5){
       updt(5)
     }else{
       updt(6)
     }
   })
   
   observeEvent(input$savecar, {
     if(input$RQC1 == 1){
        sav(1)
     }else if(input$RQC1 == 2){
        sav(2)
     }else if(input$RQC1 == 3){
        sav(3)
     }else if(input$RQC1 == 4){
        sav(4)
     }else if(input$RQC1 == 5){
        sav(5)
     }else{
        sav(6)
     }
   })
   
   output$image1 <- renderImage({
      return(list(src = "map.png", contentType = "image/png", alt = "map"))
   }, deleteFile = FALSE)
   
   tableMat <- matrix(0, ncol = 3, nrow = 7, dimnames = list(c("As","Cd","Cr6+","Cu","Pb","Hg","River Volume"),c("Number of times","Cost per Liter(Php)","Weight for all stations(kg/L)")))
   
   observeEvent(input$run, {
     if(is.null(input$RivStations)){
       output$NULLcbox <- renderText("No River Station Selected! Please check at least one.")
       output$table <- renderTable(tableMat, digits = 4)
     }else{
       output$NULLcbox <- renderText("")
       splt <- strsplit(input$RivStations, " ")
       matc$forsimplex = t(sapply(splt, as.numeric))
       n = length(matc$forsimplex)
       dtfs = c()
       for(i in 1:6){
         res = n*matc$matcar[i,2]
         dtfs = c(dtfs,res)
       }
       dtfs <- diag(dtfs)
       sumAs = 0
       sumCd = 0
       sumCr = 0
       sumCu = 0
       sumPb = 0
       sumHg = 0
       
       if(input$goal == 'min'){
         for(i in 1:n){
           sumAs = sum(sumAs, matc$matcas[(matc$forsimplex[1,i]),1])
         }
         for(i in 1:n){
           sumCd = sum(sumCd, matc$matcas[(matc$forsimplex[1,i]),2])
         }
         for(i in 1:n){
           sumCr = sum(sumCr, matc$matcas[(matc$forsimplex[1,i]),3])
         }
         for(i in 1:n){
           sumCu = sum(sumCu, matc$matcas[(matc$forsimplex[1,i]),4])
         }
         for(i in 1:n){
           sumPb = sum(sumPb, matc$matcas[(matc$forsimplex[1,i]),5])
         }
         for(i in 1:n){
           sumHg = sum(sumHg, matc$matcas[(matc$forsimplex[1,i]),6])
         }
       }else{
         for(i in 1:n){
           sumAs = sum(sumAs, matc$matcas[(matc$forsimplex[1,i]),1])
         }
         for(i in 1:n){
           sumCd = sum(sumCd, matc$matcas[(matc$forsimplex[1,i]),2])
         }
         for(i in 1:n){
           sumCr = sum(sumCr, matc$matcas[(matc$forsimplex[1,i]),3])
         }
         for(i in 1:n){
           sumCu = sum(sumCu, matc$matcas[(matc$forsimplex[1,i]),4])
         }
         for(i in 1:n){
           sumPb = sum(sumPb, matc$matcas[(matc$forsimplex[1,i]),5])
         }
         for(i in 1:n){
           sumHg = sum(sumHg, matc$matcas[(matc$forsimplex[1,i]),6])
         }
         
         sumAs = sum(sumAs, (0.05*n))
         sumCd = sum(sumCd, (0.01*n))
         sumCr = sum(sumCr, (0.05*n))
         sumCu = sum(sumCu, (0.05*n))
         sumPb = sum(sumPb, (0.06*n))
         sumHg = sum(sumHg, (0.002*n))
       }
      
       z = c(sumAs,sumCd,sumCr,sumCu,sumPb,sumHg)
       dtfs <- rbind(dtfs,z)
       dtfs[nrow(dtfs),] <- -1*dtfs[nrow(dtfs),]
       dtfs <- cbind(dtfs, diag(nrow(dtfs)))
       sol = matrix(0,nrow = 7)
       for(i in 1:7){
         if(i < 7){
           sol[i,1] = matc$matcar[i,1]
         }else{
           sol[i,1] = 0
         }
       }
       
       dtfs <- cbind(dtfs, sol)
       tableRes = SimplexMethod(dtfs)
       for(i in 1:6){
          tableMat[i,1] = tableRes[7,(i+6)]
       }
       costlist = c(0.06,0.35,1.47,0.47,0.23,0.02)
       amountlist = c(0.04,0.08,0.02,0.04,0.03,0.45)
       for(i in 1:6){
         tableMat[i,2] = tableRes[7,(i+6)]*costlist[i]
       }
       
       sumVol = 0
       for(i in 1:n){
         sumVol = sum(sumVol, volmat[(matc$forsimplex[1,i]),1])
       }
       print(dtfs)
       tableMat[7,1] = sumVol

       for(i in 1:6){
         tableMat[i,3] = (tableRes[7,(i+6)]*amountlist[i]*sumVol) / 1000000
       }
       output$table <- renderTable(tableMat, digits = 4)
     }
   })
  }
)